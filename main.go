package main

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/alexsasharegan/dotenv"
	"github.com/golang/freetype/truetype"
	"github.com/golang/protobuf/proto"
	_ "github.com/lib/pq"
	"github.com/mbecker/gpxs/geo"
	"github.com/mbecker/gpxs/gpxs"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"
	_ "github.com/sendgrid/rest"
	sendgrid "github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"gitlab.com/mbecker/gpxs-amqp-lib"
	"gitlab.com/mbecker/gpxs-proto/apiv9"
)

// EnvParam represents the environment parameters set for this application to run.
type EnvParam struct {
	AmqpUser                  string
	AmqpPassword              string
	AmqpURL                   string
	AmqpQueue                 string
	MapsDirectory             string
	DbConnection              string
	DefaultGpxAndActivityName string
}

// IsValid checks if the struct EnvParam is valid
func (env *EnvParam) isValid() (bool, []error) {
	errorsSlice := []error{}
	isValid := true
	if len(env.DefaultGpxAndActivityName) == 0 {
		errorsSlice = append(errorsSlice, errors.New("DefaultGpxAndActivityName not set"))
		isValid = false
	}
	if len(env.DbConnection) == 0 {
		errorsSlice = append(errorsSlice, errors.New("DB_CONNECTION not set"))
		isValid = false
	}
	if len(env.AmqpUser) == 0 {
		errorsSlice = append(errorsSlice, errors.New("AMQP_USER not set"))
		isValid = false
	}
	if len(env.AmqpPassword) == 0 {
		errorsSlice = append(errorsSlice, errors.New("AMQP_PASSWORD not set"))
		isValid = false
	}
	if len(env.AmqpURL) == 0 {
		errorsSlice = append(errorsSlice, errors.New("AMQP_URL not set"))
		isValid = false
	}
	if len(env.AmqpQueue) == 0 {
		errorsSlice = append(errorsSlice, errors.New("AMQP_QUEUE not set"))
		isValid = false
	}
	if len(env.AmqpQueue) == 0 {
		errorsSlice = append(errorsSlice, errors.New("AMQP_QUEUE not set"))
		isValid = false
	}
	if len(env.MapsDirectory) == 0 {
		errorsSlice = append(errorsSlice, errors.New("MAPS_DIRECTORY not set"))
		isValid = false
	}
	return isValid, errorsSlice
}

var envParams EnvParam
var currentDirectory string

var mapDirectory string
var vincenty = geo.Vincenty{
	Name: "Vincenty_SD",
	ShouldStandardDeviationBeUsed: true,
	SigmaMultiplier:               2.57583, // - 99%; 3.29053, // - 99.9%
	OneDegree:                     1000.0 * 10000.8 / 90.0,
	EarthRadius:                   6378137, // WGS-84 ellipsoid; See https://en.wikipedia.org/wiki/World_Geodetic_System
	Flattening:                    1 / 298.257223563,
	SemiMinorAxisB:                6356752.314245,
	Epsilon:                       1e-12,
	MaxIterations:                 200,
}

const fontSubDirectory string = "ttf"

var fontDirectory string
var applicationName = "gpxs-maps"
var applicationID string

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatal().Err(err).Msg(msg)
	}
}
func failOnErrors(key string, errs []error, msg string) {
	if len(errs) > 0 {
		log.Fatal().Errs(key, errs).Msg(msg)
	}
}

func init() {
	applicationID = xid.New().String()
	log.Logger = log.With().Timestamp().Str("applicationName", applicationName).Str("applicationID", applicationID).Logger() // Caller()
	// zerolog.TimeFieldFormat = "" -- Use for timestamp
	log.Info().Msg("Initialized application started")

	// Initialize environment paramaeters
	err := dotenv.Load()
	if err != nil {
		failOnError(err, "dotenv error")
	}
	envParams.AmqpURL = os.Getenv("AMQP_URL")
	envParams.AmqpUser = os.Getenv("AMQP_USER")
	envParams.AmqpPassword = os.Getenv("AMQP_PASSWORD")
	envParams.AmqpQueue = os.Getenv("AMQP_QUEUE")
	envParams.MapsDirectory = os.Getenv("MAPS_DIRECTORY")
	envParams.DbConnection = os.Getenv("DB_CONNECTION")
	envParams.DefaultGpxAndActivityName = os.Getenv("DEFAULT_GPX_AND_ACTIVITY_NAME")

	if isValid, errs := envParams.isValid(); isValid != true {
		failOnErrors("errors", errs, "ENV Parameters must be set")
	}

	// Initialize current directory
	currentDirectory, err = os.Getwd()
	failOnError(err, "The current directory couldn't be loaded")

	mapDirectory = filepath.Join(envParams.MapsDirectory)
	err = CreateDirIfNotExist(mapDirectory)
	failOnError(err, fmt.Sprintf("The directoy '%s' couldn't be created", mapDirectory))

	fontDirectory = filepath.Join(currentDirectory, fontSubDirectory)
	fontActivityTypeFilePath := filepath.Join(fontDirectory, "FTY DELIRIUM NCV.ttf")
	fontActivityStatsTypeFilePath := filepath.Join(currentDirectory, "ttf", "Roboto-Black.ttf")
	fontActivityDateFilePath := filepath.Join(currentDirectory, "ttf", "Roboto-Bold.ttf")
	fontLogoFilePath := filepath.Join(currentDirectory, "ttf", "Roboto-Bold.ttf")

	// Font: Activity Type
	fontFile, err := ioutil.ReadFile(fontActivityTypeFilePath)
	if err != nil {
		failOnError(err, "The font 'activity' couldn't be loaded")
	}
	fontActivityType, err := truetype.Parse(fontFile)
	if err != nil {
		failOnError(err, "The font 'activity' couldn't be parsed")
	}
	// Font: Activity Stats
	fontFile, err = ioutil.ReadFile(fontActivityStatsTypeFilePath)
	if err != nil {
		failOnError(err, "The font 'stats' couldn't be loaded")
	}
	fontActivityStats, err := truetype.Parse(fontFile)
	if err != nil {
		failOnError(err, "The font 'stats' couldn't be parsed")
	}

	// Font: Activity Data
	fontFile, err = ioutil.ReadFile(fontActivityDateFilePath)
	if err != nil {
		failOnError(err, "The font 'date' couldn't be loaded")
	}
	fontActivityDate, err := truetype.Parse(fontFile)
	if err != nil {
		failOnError(err, "The font 'stats' couldn't be parsed")
	}

	// font: Logo
	fontFile, err = ioutil.ReadFile(fontLogoFilePath)
	if err != nil {
		failOnError(err, "The font 'logo' couldn't be loaded")
	}
	fontLogo, err := truetype.Parse(fontFile)
	if err != nil {
		failOnError(err, "The font 'logo' couldn't be parsed")
	}

	for index := 0; index < len(ImagePresets); index++ {
		ImagePresets[index].fontActivityType = fontActivityType
		ImagePresets[index].fontActivityStats = fontActivityStats
		ImagePresets[index].fontActivityDate = fontActivityDate
		ImagePresets[index].fontLogo = fontLogo
	}
	log.Info().Msg("Initialized application finished")
}

func main() {

	// Initialize postgresql connection

	/**
	 * TODO:
	 * - [] Read and update how connection could be new established if it's gone
	 */
	db, err := sql.Open("postgres", envParams.DbConnection)
	db.SetMaxOpenConns(20)
	db.SetMaxIdleConns(20)
	if err != nil {
		failOnError(err, "DB Connection couldn't be established")
	}
	defer db.Close()
	err = db.Ping()
	failOnError(err, "DB Connection Ping error")

	// Initialize AMQP Connection
	amqpConnectionString := fmt.Sprintf("amqp://%s:%s@%s/", envParams.AmqpUser, envParams.AmqpPassword, envParams.AmqpURL)
	queue := gpxsamqplib.New(envParams.AmqpQueue, amqpConnectionString)

	// Graceful shutdown: Close all existing / open connections
	go func() {
		sigint := make(chan os.Signal, 1)

		// interrupt signal sent from terminal
		signal.Notify(sigint, os.Interrupt)
		// sigterm signal sent from kubernetes
		signal.Notify(sigint, syscall.SIGTERM)
		signal.Notify(sigint, syscall.SIGINT)

		<-sigint

		err := queue.Close()
		if err != nil {
			log.Error().Err(err).Msg("AMQP connection couldn't be closed")
		}
		err = db.Close()
		if err != nil {
			log.Error().Err(err).Msg("DB connection couldn't be closed")
		}
		log.Info().Msgf("Caught sig: %+v", sigint)
		log.Info().Msg("Wait for 2 second to finish processing")
		time.Sleep(2 * time.Second)
		os.Exit(0)
	}()

	for {
		for !queue.IsConnected || queue.IsClosing {
			time.Sleep(gpxsamqplib.ReconnectDelay)
		}

		err := queue.SetQos(1, 0, false)
		failOnError(err, "Failed to set QoS")

		// consumer, err := queue.Channel.Consume(
		// 	envParams.AmqpQueue, // queue
		// 	"",                  // consumer
		// 	false,               // auto-ack
		// 	false,               // exclusive
		// 	false,               // no-local
		// 	false,               // no-wait
		// 	nil,                 // args
		// )

		queue.Consume(func(body []byte) {
			activityMessage := &apiv9.Activity{}
			err := proto.Unmarshal(body, activityMessage)
			if err != nil {
				log.Info().Msg("AMQP message couldn't be marshaled as GpxType")
				return
			}

			switch activityMessage.GpxType {
			case apiv9.GpxType_Gpxs:
				fmt.Println("--> Handle GPX Message")
				go handleGpxProcess(db, activityMessage)
			case apiv9.GpxType_Strava:
				fmt.Println("--> Handle Strava Message")
				go handleStravaMessage(db, activityMessage)
			}

		})

		log.Info().Msg(" [*] Waiting for logs. To exit press CTRL+C")

		select {
		case <-queue.Done:
			log.Info().Str("func", "amqp").Msg("DONE ####")
			return
		case <-queue.NotifyClose:
			log.Info().Str("func", "amqp").Msg("NotifyClose !!!!!")
		}

	}
}

func sendTransactionalMail(imageSrc string, imageHref string) {
	request := sendgrid.GetRequest("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", "/v3/mail/send", "https://api.sendgrid.com")
	request.Method = "POST"
	mailBody := fmt.Sprintf(`
	{
		"from":{
		   "email":"maps@mobility.digital"
		},
		"personalizations":[
		   {
			  "to":[
				 {
					"email":"mats.becker@gmail.com"
				 }
			  ],
			  "dynamic_template_data":{
				 "map_image":"%s",
				 "map_link":"%s"
			   }
		   }
		],
		"template_id":"d-b438d647ca2e4a60a9152b3b3f98f43a"
	 }
	`, imageSrc, imageHref)
	request.Body = []byte(mailBody)

	response, err := sendgrid.API(request)
	if err != nil {
		fmt.Println("--- MAIL SUCCESS ---")
		fmt.Println(err)
	} else {
		fmt.Println("--- MAIL SUCCESS ---")
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)
	}
}

func sendMail() {
	from := mail.NewEmail("gpxs maps", "test@mobility.digital")
	subject := "Sending with SendGrid is Fun"
	to := mail.NewEmail("Mats Becker", "mats.becker@gmail.com")
	plainTextContent := "and easy to do anywhere, even with Go"
	htmlContent := "<strong>and easy to do anywhere, even with Go</strong>"
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient("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")
	response, err := client.Send(message)
	if err != nil {
		fmt.Println("--- MAIL SUCCESS ---")
		fmt.Println(err)
	} else {
		fmt.Println("--- MAIL SUCCESS ---")
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)
	}
}

func handleStravaMessage(db *sql.DB, activityMessage *apiv9.Activity) {
	fmt.Println(":: Handle Strave Message ::")

	mapID := activityMessage.GetMapID()
	uuid, err := uuid.FromString(activityMessage.GetUuid())
	if err != nil || len(uuid) == 0 {
		log.Error().Err(err).Str("mapID", mapID).Msg("Strava Message: UUID is not a valid uuid")
		return
	}
	activityID := activityMessage.GetActivityID()

	stravaID := activityMessage.StravaActivity.GetStravaID()
	points := activityMessage.StravaActivity.GetPoints()
	movingTime := activityMessage.StravaActivity.GetMovingTime()
	distance := activityMessage.StravaActivity.GetDistance()

	activityName := activityMessage.GetActivityName()
	activityType := activityMessage.GetActivityType()
	activityColor := activityMessage.GetActivityColor() // Optional
	imagePresetType := activityMessage.GetImagePresetType()
	tileProviderType := activityMessage.GetTileProviderType()
	isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity := getPrivacy(activityMessage.GetPrivacyType())

	// createdAt := activityMessage.GetCreatedAt()

	// ToDo: Check that imagePresetType and tileProvider Type exists (teh value should > 0 && < 11? - max value of each type)
	if activityID == 0 || len(mapID) == 0 || stravaID == 0 || len(points) == 0 || movingTime == 0 || len(activityName) == 0 {
		fmt.Println("activityID: ", activityID)
		fmt.Println("len(mapID): ", len(mapID))
		fmt.Println("stravaID: ", stravaID)
		fmt.Println("len(points): ", len(points))
		fmt.Println("movingTime: ", movingTime)
		fmt.Println("len(activityName): ", len(activityName))
		fmt.Println("activityType: ", activityType)
		fmt.Println("imagePresetType: ", imagePresetType)
		fmt.Println("tileProviderType: ", tileProviderType)
		log.Info().Msg("Any of the values are either nil or 0")
		return
	}

	fmt.Println("activityID: ", activityID)
	fmt.Println("mapID: ", mapID)
	fmt.Println("activityColor: ", activityColor)

	// ImagePreset: Default value =
	imagePreset := GetImagePreset(activityMessage.GetImagePresetType())

	// Tileprovider: Default value = GpxMessage_TileProviderCartoDark
	tileProvider := activityMessage.GetTileProviderType()
	imagePreset.tileProvider = TileProviders[tileProvider]

	t01, _ := time.ParseDuration(fmt.Sprintf("%ds", movingTime))
	labelActivityStats := fmt.Sprintf("%.2fkm in %s", distance/1000, t01)

	log.Info().Msg("--> Create Strava Map")
	activityImages, err := createStravaMap(
		uuid,
		activityID,
		mapID,
		points,
		labelActivityStats,
		activityColor,
		activityType,
		imagePreset,
		mapDirectory,
	)

	if err != nil {
		return
	}

	log.Info().Msg("--> Create Strava SQL")
	privacy := Privacy{
		IsPrivacyCommunity: isPrivacyCommunity,
		IsPrivacyPrivate:   isPrivacyPrivate,
		IsPrivacyPublic:    isPrivacyPublic,
	}
	id, err := sqlStrava(db, activityID, mapID, uuid, activityImages, &privacy)
	if err != nil {
		log.Error().Err(err).Msg("SQL Error")
		return
	}

	imageServer := "https://api.mobility.digital/static/"
	imageSrc := fmt.Sprintf("%s/%s", imageServer, activityImages.Resized.FileName)
	imageHref := fmt.Sprintf("%s/%s", imageServer, activityImages.Original.FileName)
	go sendTransactionalMail(imageSrc, imageHref)

	fmt.Println("--> Map created successfull with id: ", id)

}

func handleGpxProcess(db *sql.DB, activityMessage *apiv9.Activity) {

	mapID := activityMessage.GetMapID()
	if len(mapID) == 0 {
		log.Warn().Msg("Activity Message: mapID length == 0")
		return
	}
	defer timeTrack(time.Now(), "handleGpxProcess", mapID)

	uuid, err := uuid.FromString(activityMessage.GetUuid())
	if err != nil || len(uuid) == 0 {
		log.Warn().Str("xid", mapID).Msg("GPX Message: UUID is not a valid uuid")
		return
	}
	activityID := activityMessage.GetActivityID()
	if activityID == 0 {
		log.Warn().Msg("Activity Message: activityID == 0")
		return
	}

	fileName := activityMessage.GpxsActivity.GetFilename()

	activityName := activityMessage.GetActivityName()
	activityColor := activityMessage.GetActivityColor()
	// Privacy
	isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity := getPrivacy(activityMessage.GetPrivacyType())

	// ActivityType: Default value = GpxMessage_activityTypeDefault
	activityType := activityMessage.GetActivityType()

	// ImagePreset: Default value =
	imagePreset := GetImagePreset(activityMessage.GetImagePresetType())

	// Tileprovider: Default value = GpxMessage_TileProviderCartoDark
	tileProvider := activityMessage.GetTileProviderType()
	imagePreset.tileProvider = TileProviders[tileProvider]

	createdAt := activityMessage.GetCreatedAt()

	if len(fileName) == 0 {
		log.Warn().Str("xid", mapID).Msg("Activity Message: fileName length == 0")
		return // errors.New("GPX Message: Filename's length == 0")
	}

	if createdAt == nil {
		log.Warn().Str("xid", mapID).Msg("Activity Message: Created_At nil")
		return // errors.New("GPX Message: Created_At's length == 0")
	}

	gpxDoc, err := readGpxFile(fileName, mapID)
	if err != nil {
		log.Error().Err(err).Msg("Gpx File couldn't be parsed")
		return //
	}

	// User has not select an option for "activity name" and/or "activity type"
	if len(activityName) == 0 {
		activityName = gpxDoc.Name
	}

	if activityType == apiv9.ActivityType_Default {
		activityType = getActivityTypeString(gpxDoc.Type)
	}

	/**
	 * TODO:
	 * - [] Update filename (directory is mounted; directory "gpx" should be at top level)
	 */

	// Filepath: Get the base file name ("/dir1/dir2/xid_timestamp.gpx" -> "xid_timestamp.gpx")
	baseFileName := filepath.Base(fileName)                                                   // "xid_timestamp.gpx"
	baseFileNameWithExt := fmt.Sprintf("%s.%s", baseFileName, imagePreset.imageType.String()) // "xid_timestamp.gpx.jpeg"
	baseResizedFileNameWithExt := fmt.Sprintf("%s_resized.jpeg", baseFileName)                // "xid_timestamp.gpx_resized.jpeg"
	baseThumbFileNameWithExt := fmt.Sprintf("%s_thumb.jpeg", baseFileName)                    // "xid_timestamp.gpx_thumb.jpeg"
	mapImageFilePath := filepath.Join(mapDirectory, baseFileNameWithExt)                      // "/maps/xid_timestamp.gpx.jpeg"
	mapImageResizedFilePath := filepath.Join(mapDirectory, baseResizedFileNameWithExt)        // "/maps/xid_timestamp.gpx_resized.jpeg"
	mapThumbFilePath := filepath.Join(mapDirectory, baseThumbFileNameWithExt)                 // "/maps/xid_timestamp.gpx_thumb.jpeg"

	// Labels Activity Stats & Date: "69.02km in 2h45min23s" & "29.11.2018"
	t01, _ := time.ParseDuration(fmt.Sprintf("%ds", int64(gpxDoc.MovementStats.MovingData.Duration)))
	labelActivityStats := fmt.Sprintf("%.2fkm in %s", gpxDoc.MovementStats.OverallData.Distance/1000, t01)
	labelActivityDate := fmt.Sprintf("%s", gpxDoc.Timestamp)

	// Eventually consistency: The image could not yet be created but we insert the entry to the DB
	go createMap(
		gpxDoc.Tracks,           // Reference to the gpx tracks
		mapImageFilePath,        // Map image file path with extension jpeg|png
		mapImageResizedFilePath, // Resized Map image path with extension jpeg|png
		"",           // Thumb Map Image path with extension jpeg|png
		imagePreset,  // ImagePreset
		activityType, // Activity Type * (for color if color is empty)
		activityColor,
		labelActivityStats, // Label Activiy Stats
		labelActivityDate,  // Label Acitivy Date
		mapID,              // xid
	)

	go createThumb(
		gpxDoc.Tracks,
		mapThumbFilePath,
		imagePreset,
		activityType,
		mapID,
	)

	/**
	 * TODO:
	 * - [x] CreateMpas must return: activityType,
	 * - [] The function createThumb could generate the thumb image later than the sql statements; the
	 */

	err = sqlInserts(
		db,
		activityID,
		mapID,
		uuid,
		gpxDoc,
		activityName,
		envParams.DefaultGpxAndActivityName,
		int(activityType),
		isPrivacyPublic,
		isPrivacyCommunity,
		isPrivacyPrivate,
		baseFileNameWithExt,
		mapImageFilePath,
		imagePreset.imageWidth,
		imagePreset.imageHeight,
		imagePreset.imageType.String(),
		imagePreset.Name,
		baseResizedFileNameWithExt,
		mapImageResizedFilePath,
		imagePreset.resizedImage.width,
		imagePreset.resizedImage.height,
		baseThumbFileNameWithExt,
		mapThumbFilePath,
		imagePreset.thumbImage.width,
		imagePreset.thumbImage.height,
		vincenty.String(),
	)
	if err != nil {
		log.Error().Err(err).Str("xid", mapID).Str("uuid", uuid.String()).Msg("SQL Error")
	}

	// return nil
}

func readGpxFile(filePath string, mapID string) (*geo.GPX, error) {
	defer timeTrack(time.Now(), "readGpxFile", mapID)
	return gpxs.ParseFile(filepath.Join(filePath), &vincenty)
}

func replaceFilename(gpxs *geo.GPX, imagePreset ImagePreset) string {
	mapFilename := fmt.Sprintf("%s##%s##%s--%s", gpxs.Timestamp, gpxs.Name, imagePreset.Name, imagePreset.tileProvider.Name)
	mapFilename = strings.Replace(mapFilename, " ", "_", -1)
	mapFilename = strings.Replace(mapFilename, ":", "-", -1)
	mapFilename = strings.Replace(mapFilename, "|", "-", -1)
	mapFilename = strings.Replace(addslashes(mapFilename), "\\", "-", -1)
	return mapFilename
}

// addslashes()
func addslashes(str string) string {
	var buf bytes.Buffer
	for _, char := range str {
		switch char {
		case '\'':
			buf.WriteRune('\\')
		}
		buf.WriteRune(char)
	}
	return buf.String()
}

func getPrivacy(privacy []apiv9.PrivacyType) (bool, bool, bool) {
	var isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity bool
	if privacy == nil {
		isPrivacyPrivate = false
		isPrivacyPublic = true
		isPrivacyCommunity = true
	} else {
		for _, el := range privacy {
			if el == apiv9.PrivacyType_Private {
				isPrivacyPrivate = true
				isPrivacyCommunity = false
				isPrivacyPublic = false
				return isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity
			}
			if el == apiv9.PrivacyType_Community {
				isPrivacyPrivate = false
				isPrivacyCommunity = true
			}
			if el == apiv9.PrivacyType_Community {
				isPrivacyPrivate = false
				isPrivacyPublic = true
			}
		}
	}

	return isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity
}
