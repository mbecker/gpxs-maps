package main

import (
	"image/color"

	"gitlab.com/mbecker/gpxs-proto/apiv9"
)

// var ActivityTypeLabels = [...]string{
// 	"Running", // 0
// 	"Cycling", // 1
// 	"Running", // 2
// 	"Running", // 3
// 	"Hiking",  // 4
// 	"Running", // 5
// 	"Running", // 6
// 	"Running", // 7
// 	"Running", // 8
// 	"Running", // 9
// 	"Walking", // 10
// }

var ActivityTypeColors = [...]color.Color{
	color.RGBA{242, 60, 80, 0xff},   // Strava Type 0: Default? -> Assumption: The gpx is a running activity; so use the running color from 9
	color.RGBA{255, 203, 5, 0xff},   // Strava Type 1: Cycling - yellow
	color.RGBA{255, 255, 255, 0xff}, // Strava Type 2: ? - white
	color.RGBA{255, 255, 255, 0xff}, // Strava Type 3: ? - white
	color.RGBA{6, 149, 171, 0xff},   // Strava Type 4: Hiking - blue
	color.RGBA{255, 255, 255, 0xff}, // Strava Type 5: ? - white
	color.RGBA{255, 255, 255, 0xff}, // Strava Type 6: ? - white
	color.RGBA{255, 255, 255, 0xff}, // Strava Type 7: ? - white
	color.RGBA{255, 255, 255, 0xff}, // Strava Type 8: ? - white
	color.RGBA{242, 60, 80, 0xff},   // Strava Type 9: Running - pink/red
	color.RGBA{69, 196, 139, 0xff},  // Strava Type 10: Walking - green
}

var Colors = map[string]color.RGBA{
	"white": color.RGBA{255, 255, 255, 0x40},
}

func getActivityTypeInt(i int64) apiv9.ActivityType {
	switch i {
	case 0:
		return apiv9.ActivityType_Default
	case 1:
		return apiv9.ActivityType_Cycling
	case 2:
		return apiv9.ActivityType_Default2
	case 3:
		return apiv9.ActivityType_Default3
	case 4:
		return apiv9.ActivityType_Hiking
	case 5:
		return apiv9.ActivityType_Default5
	case 6:
		return apiv9.ActivityType_Default6
	case 7:
		return apiv9.ActivityType_Default7
	case 8:
		return apiv9.ActivityType_Default8
	case 9:
		return apiv9.ActivityType_Running
	case 10:
		return apiv9.ActivityType_Walking
	default:
		return apiv9.ActivityType_Running
	}
}

func getActivityTypeString(activityType string) apiv9.ActivityType {
	switch activityType {
	case "0":
		return apiv9.ActivityType_Default
	case "1":
		return apiv9.ActivityType_Cycling
	case "2":
		return apiv9.ActivityType_Default2
	case "3":
		return apiv9.ActivityType_Default3
	case "4":
		return apiv9.ActivityType_Hiking
	case "5":
		return apiv9.ActivityType_Default5
	case "6":
		return apiv9.ActivityType_Default6
	case "7":
		return apiv9.ActivityType_Default7
	case "8":
		return apiv9.ActivityType_Default8
	case "9":
		return apiv9.ActivityType_Running
	case "10":
		return apiv9.ActivityType_Walking
	default:
		return apiv9.ActivityType_Running
	}
}
