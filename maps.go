package main

import (
	"errors"
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"os"
	"path/filepath"
	"time"

	staticmaps "github.com/flopp/go-staticmaps"
	"github.com/fogleman/gg"
	trueytpe "github.com/golang/freetype/truetype"
	"github.com/golang/geo/s2"
	colorful "github.com/lucasb-eyer/go-colorful"
	"github.com/mbecker/gpxs/geo"
	"github.com/nfnt/resize"
	"github.com/satori/go.uuid"
	"gitlab.com/mbecker/gpxs-proto/apiv9"
)

//ImageLogo is used for the map images
const ImageLogo = "gpxs"

func createStravaMap(
	uuid uuid.UUID,
	activityID int64,
	mapID string,
	points []*apiv9.Point,
	labelActivityStats string,
	activityColor string,
	activityType apiv9.ActivityType,
	imagePreset ImagePreset,
	mapDirectory string,
) (*ActivityImages, error) {

	mapImage, err := createImageForStrava(mapID, points, labelActivityStats, activityColor, activityType, imagePreset, imagePreset.imageWidth, imagePreset.imageHeight)
	if err != nil {
		return nil, err
	}
	fmt.Println("create strava map - activityID: ", activityID)
	fileName := fmt.Sprintf("%v_%s", activityID, mapID)
	fmt.Println("create strava map - fileName: ", fileName)
	fmt.Println("create strava map - ImagePreset: ", imagePreset.Name)
	fmt.Println("create strava map - ImagePreset: ", imagePreset.tileProvider.Name)

	activityImages := ActivityImages{
		ImagePreset:      &imagePreset,
		ActivityTpe:      int32(activityType),
		ActivityTypeName: apiv9.ActivityType_name[int32(activityType)],
		Filename:         fileName,
		Original: ActivityImagesStatus{
			FileName: fmt.Sprintf("%s.%s", fileName, imagePreset.imageType.String()),
			Path:     filepath.Join(mapDirectory, fmt.Sprintf("%s.%s", fileName, imagePreset.imageType.String())),
			Width:    imagePreset.imageWidth,
			Height:   imagePreset.imageHeight,
		},
		Resized: ActivityImagesStatus{
			FileName: fmt.Sprintf("%s_resized.jpeg", fileName),
			Path:     filepath.Join(mapDirectory, fmt.Sprintf("%s_resized.jpeg", fileName)),
			Width:    imagePreset.resizedImage.width,
			Height:   imagePreset.resizedImage.height,
		},
		Thumb: ActivityImagesStatus{
			FileName: fmt.Sprintf("%s_thumb.jpeg", fileName),
			Path:     filepath.Join(mapDirectory, fmt.Sprintf("%s_thumb.jpeg", fileName)),
			Width:    imagePreset.thumbImage.width,
			Height:   imagePreset.thumbImage.height,
		},
		CreatedAt: time.Now(),
	}

	var saveErr error

	// Original images
	switch imagePreset.imageType {
	case 0:
		saveErr = SaveJPEG(activityImages.Original.Path, mapImage, 100)
	case 1:
		saveErr = gg.SavePNG(activityImages.Original.Path, mapImage)
	default:
		saveErr = errors.New("No ImageType given")
	}
	if saveErr == nil {
		activityImages.Original.Saved = true
	}

	// Resized image
	resizedImage := resize.Thumbnail(800, 600, mapImage, resize.Bilinear)
	saveErr = SaveJPEG(activityImages.Resized.Path, resizedImage, 75)
	if saveErr == nil {
		activityImages.Resized.Saved = true
	}
	/**
	 * TODO:
	 * - [x] Create a thumb with the correct size bit with a readable font size
	 */
	// Thumb image (create additinonal image with new resoultion to have a nicer image)
	// mapImage, err = createImageForStrava(mapID, points, labelActivityStats, activityColor, activityType, imagePreset, imagePreset.thumbImage.width*5, imagePreset.thumbImage.height*5)
	// if err != nil {
	// 	return &activityImages, err
	// }
	thumbImage := resize.Thumbnail(uint(imagePreset.thumbImage.width), uint(imagePreset.thumbImage.height), mapImage, resize.Bilinear)
	saveErr = SaveJPEG(activityImages.Thumb.Path, thumbImage, 75)
	if saveErr == nil {
		activityImages.Thumb.Saved = true
	}

	return &activityImages, nil

}

func createImageForStrava(
	mapID string,
	points []*apiv9.Point,
	labelActivityStats string,
	activityColor string,
	activityType apiv9.ActivityType,
	imagePreset ImagePreset,
	imageWidth float64,
	imageHeight float64,
) (image.Image, error) {
	defer timeTrack(time.Now(), "createImageForStrava", mapID)
	if len(points) == 0 {
		return nil, errors.New("No Points exists")
	}

	/*
	 * Create image
	 */
	ctx := staticmaps.NewContext()
	ctx.SetSize(int(imageWidth), int(imageHeight))
	ctx.SetTileProvider(imagePreset.tileProvider.TileProvider)
	paths := make([]*staticmaps.Path, 0, 0)

	//  Label: Text (if the activity tpye is 0 the use "Running")
	var mapActivityTypeLabel string
	switch activityType {
	case apiv9.ActivityType_Default:
		mapActivityTypeLabel = apiv9.ActivityType_Running.String()
	default:
		mapActivityTypeLabel = activityType.String()
	}

	// Label / Path: Color
	var mapActivityColor color.Color
	mapActivityColor, err := colorful.Hex(activityColor)
	if err != nil {
		mapActivityColor = imagePreset.tileProvider.activityTypeColors[activityType]
	}

	// Path: Add path to image
	p := new(staticmaps.Path)
	p.Color = mapActivityColor
	p.Weight = imagePreset.pathWeight
	for _, pt := range points {
		p.Positions = append(p.Positions, s2.LatLngFromDegrees(pt.Lat, pt.Long))
	}
	if len(p.Positions) > 0 {
		paths = append(paths, p)
	}
	ctx.AddPath(p)

	// Image: Render Image to get the current ctx
	img, err := ctx.Render()
	if err != nil {
		return nil, err
	}

	// Create labels (activity type, stats, logo)
	ggCtx := gg.NewContextForImage(img)
	faceActivityType := trueytpe.NewFace(imagePreset.fontActivityType, imagePreset.fontActivityTypeOptions)
	faceActivityStats := trueytpe.NewFace(imagePreset.fontActivityType, imagePreset.fontActivityTypeOptions)
	// faceActivityDate := trueytpe.NewFace(imagePreset.fontActivityDate, imagePreset.fontActivityDateOptions)
	faceLogo := trueytpe.NewFace(imagePreset.fontLogo, imagePreset.fontLogoOptions)

	// Activity Type - Top Left
	ggCtx.SetFontFace(faceActivityType)
	ggCtx.SetColor(mapActivityColor)
	_, y := ggCtx.MeasureString(mapActivityTypeLabel)
	/**
	 * NOTE:
	 * The start point(x,y) is left bottom of the string
	 */
	ggCtx.DrawString(
		mapActivityTypeLabel,
		imagePreset.imagePaddingLeft,
		imagePreset.imagePaddingTop+y,
	)

	// Activity Stats - Top Left; below Activity Type
	ggCtx.SetFontFace(faceActivityStats)
	ggCtx.SetColor(imagePreset.tileProvider.fontActivityStatsColor)
	_, yActivityStats := ggCtx.MeasureString(labelActivityStats)
	ggCtx.DrawString(
		labelActivityStats,
		imagePreset.imagePaddingLeft,
		imagePreset.imagePaddingTop+yActivityStats+y,
	)

	// Activity Date - Top Right
	// if imagePreset.shouldActivityDateBePrinted {
	// 	ggCtx.SetFontFace(faceActivityDate)
	// 	ggCtx.SetColor(imagePreset.tileProvider.fontActivityDateColor)
	// 	xDate, _ := ggCtx.MeasureString(labelActivityDate)
	// 	ggCtx.DrawString(
	// 		labelActivityDate,
	// 		imageWidth-imagePreset.imagePaddingRight-xDate,
	// 		(imagePreset.imagePaddingTop+y)-(y/2),
	// 	)
	// }

	// Logoname: Bottom Right
	ggCtx.SetFontFace(faceLogo)
	ggCtx.SetColor(imagePreset.tileProvider.fontLogoColor)
	x3, _ := ggCtx.MeasureString(ImageLogo)

	ggCtx.DrawString(
		ImageLogo,
		imageWidth-imagePreset.imagePaddingRight-x3,
		imageHeight-(imagePreset.imagePaddingBottom+y)+(y/2),
	)

	// Image & Resized Image
	return ggCtx.Image(), nil
}

func createMap(
	gpxTracks []geo.GPXTrack,
	mapImageFilePath string,
	mapImageResizedFilePath string,
	mapThumbResizedFilePath string,
	imagePreset ImagePreset,
	activityType apiv9.ActivityType,
	activityColor string,
	labelActivityStats string,
	labelActivityDate string,
	xid string,
) error {
	defer timeTrack(time.Now(), "createMap", xid)

	if len(gpxTracks) == 0 {
		return errors.New("No GPX Tracks exists")
	}

	ctx := staticmaps.NewContext()
	ctx.SetSize(int(imagePreset.imageWidth), int(imagePreset.imageHeight))
	ctx.SetTileProvider(imagePreset.tileProvider.TileProvider)
	paths := make([]*staticmaps.Path, 0, 0)

	/**
	 * TODO:
	 * - [] Update that each tack get it's own activityTpe and put that on the image
	 */

	mapActivityTypeLabel := activityType.String()
	var mapActivityColor color.Color
	mapActivityColor, err := colorful.Hex(activityColor)
	if err != nil {
		mapActivityColor = imagePreset.tileProvider.activityTypeColors[activityType]
	}

	for _, trk := range gpxTracks {

		for _, seg := range trk.Segments {
			p := new(staticmaps.Path)
			p.Color = mapActivityColor
			p.Weight = imagePreset.pathWeight
			for _, pt := range seg.Points {
				p.Positions = append(p.Positions, s2.LatLngFromDegrees(pt.Latitude, pt.Longitude))
				// Moving Poins on map
				// if !pt.IsMoving {
				// 	circle := new(staticmaps.Circle)
				// 	circle.Color = color.RGBA{255, 255, 255, 0xff}
				// 	circle.Fill = color.RGBA{255, 255, 255, 0xff}
				// 	circle.Position = s2.LatLngFromDegrees(pt.Latitude, pt.Longitude)
				// 	circle.Radius = 10.0
				// 	circle.Weight = 4.0
				// 	ctx.AddCircle(circle)
				// }
			}
			if len(p.Positions) > 0 {
				paths = append(paths, p)
			}
			ctx.AddPath(p)

		}

	}

	img, err := ctx.Render()
	if err != nil {
		return err
	}

	// Create label
	ggCtx := gg.NewContextForImage(img)
	faceActivityType := trueytpe.NewFace(imagePreset.fontActivityType, imagePreset.fontActivityTypeOptions)
	faceActivityStats := trueytpe.NewFace(imagePreset.fontActivityType, imagePreset.fontActivityTypeOptions)
	faceActivityDate := trueytpe.NewFace(imagePreset.fontActivityDate, imagePreset.fontActivityDateOptions)
	faceLogo := trueytpe.NewFace(imagePreset.fontLogo, imagePreset.fontLogoOptions)

	// Activity Type - Top Left
	ggCtx.SetFontFace(faceActivityType)
	ggCtx.SetColor(imagePreset.tileProvider.activityTypeColors[activityType])
	_, y := ggCtx.MeasureString(mapActivityTypeLabel)
	/**
	 * NOTE:
	 * The start point(x,y) is left bottom of the string
	 */
	ggCtx.DrawString(
		mapActivityTypeLabel,
		imagePreset.imagePaddingLeft,
		imagePreset.imagePaddingTop+y,
	)

	// Activity Stats - Top Left; below Activity Type
	ggCtx.SetFontFace(faceActivityStats)
	ggCtx.SetColor(imagePreset.tileProvider.fontActivityStatsColor)
	_, yActivityStats := ggCtx.MeasureString(labelActivityStats)
	ggCtx.DrawString(
		labelActivityStats,
		imagePreset.imagePaddingLeft,
		imagePreset.imagePaddingTop+yActivityStats+y,
	)

	// Activity Date - Top Right
	if imagePreset.shouldActivityDateBePrinted {
		ggCtx.SetFontFace(faceActivityDate)
		ggCtx.SetColor(imagePreset.tileProvider.fontActivityDateColor)
		xDate, _ := ggCtx.MeasureString(labelActivityDate)
		ggCtx.DrawString(
			labelActivityDate,
			imagePreset.imageWidth-imagePreset.imagePaddingRight-xDate,
			(imagePreset.imagePaddingTop+y)-(y/2),
		)
	}

	// Logoname: Bottom Right
	ggCtx.SetFontFace(faceLogo)
	ggCtx.SetColor(imagePreset.tileProvider.fontLogoColor)
	x3, _ := ggCtx.MeasureString(ImageLogo)

	ggCtx.DrawString(
		ImageLogo,
		imagePreset.imageWidth-imagePreset.imagePaddingRight-x3,
		imagePreset.imageHeight-(imagePreset.imagePaddingBottom+y)+(y/2),
	)

	// Image & Resized Image
	mapImage := ggCtx.Image()

	var saveErr error
	switch imagePreset.imageType {
	case 0:
		saveErr = SaveJPEG(mapImageFilePath, mapImage, 100)
	case 1:
		saveErr = gg.SavePNG(mapImageFilePath, mapImage)
	default:
		saveErr = errors.New("No ImageType given")
	}

	resizedImage := resize.Thumbnail(800, 600, mapImage, resize.Bilinear)
	saveErr = SaveJPEG(mapImageResizedFilePath, resizedImage, 75)
	thumbImage := resize.Thumbnail(164, 164, mapImage, resize.Bilinear)
	saveErr = SaveJPEG(mapThumbResizedFilePath, thumbImage, 75)
	if saveErr != nil {
		return saveErr
	}

	return nil
}

func createThumb(
	gpxTracks []geo.GPXTrack,
	mapImageFilePath string,
	imagePreset ImagePreset,
	activityType apiv9.ActivityType,
	mapID string,
) error {
	defer timeTrack(time.Now(), "createThumb", mapID)

	if len(gpxTracks) == 0 {
		return errors.New("No GPX Tracks exists")
	}

	ctx := staticmaps.NewContext()
	ctx.SetSize(int(imagePreset.thumbImage.width)*5, int(imagePreset.thumbImage.height)*5)
	ctx.SetTileProvider(imagePreset.tileProvider.TileProvider)
	paths := make([]*staticmaps.Path, 0, 0)

	for _, trk := range gpxTracks {

		for _, seg := range trk.Segments {
			p := new(staticmaps.Path)
			p.Color = imagePreset.tileProvider.activityTypeColors[activityType]
			p.Weight = imagePreset.pathWeight
			for _, pt := range seg.Points {
				p.Positions = append(p.Positions, s2.LatLngFromDegrees(pt.Latitude, pt.Longitude))
			}
			if len(p.Positions) > 0 {
				paths = append(paths, p)
			}
			ctx.AddPath(p)

		}

	}

	mapImage, err := ctx.Render()
	if err != nil {
		return err
	}
	thumbImage := resize.Thumbnail(uint(imagePreset.thumbImage.width), uint(imagePreset.thumbImage.height), mapImage, resize.Bilinear)
	saveErr := SaveJPEG(mapImageFilePath, thumbImage, 100)
	if saveErr != nil {
		return saveErr
	}
	return nil
}

//SaveJPEG saves image.Image as jpeg to the path
func SaveJPEG(path string, im image.Image, quality int) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	opt := jpeg.Options{
		Quality: quality,
	}
	return jpeg.Encode(file, im, &opt)
}
