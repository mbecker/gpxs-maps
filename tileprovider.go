package main

import (
	"image/color"

	staticmaps "github.com/flopp/go-staticmaps"
)

// NewTileProviderStamenWatercolor creates a TileProvider struct for stamens' 'toner' tile service
func NewTileProviderStamenWatercolor() *staticmaps.TileProvider {
	t := new(staticmaps.TileProvider)
	t.Name = "stamen-watercolor2"
	t.Attribution = "Maps (c) Stamen; Data (c) OSM and contributors, ODbL"
	t.TileSize = 256
	// "%[1]s" => shard, "%[2]d" => zoom, "%[3]d" => x, "%[4]d" => y
	t.URLPattern = "http://tile.stamen.com/watercolor/%[2]d/%[3]d/%[4]d.png"
	// t.URLPattern = "https://api.mapbox.com/styles/v1/mbecker/cizcjdpi9001j2so1p7h9x71t/tiles/256/%[2]d/%[3]d/%[4]d@2x?access_token=pk.eyJ1IjoibWJlY2tlciIsImEiOiJjam55Z2VzczkwNHlmM3FvMmRuYzd4Zzh1In0.LVmWiHuNBg5vb70GlAqPdw"

	t.Shards = []string{"a", "b", "c", "d"}
	return t
}

type TileProvider struct {
	Name                   string
	TileProvider           *staticmaps.TileProvider
	fontActivityTypeColor  color.Color
	fontActivityStatsColor color.Color
	fontActivityDateColor  color.Color
	fontLogoColor          color.Color
	activityTypeColors     [11]color.Color
}

var TileProviders = [13]TileProvider{
	TileProvider{
		Name:                   "CartoDark",
		TileProvider:           staticmaps.NewTileProviderCartoDark(),
		fontActivityTypeColor:  color.RGBA{0, 0, 0, 0xff},
		fontActivityStatsColor: color.RGBA{255, 255, 255, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{255, 255, 255, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "CartoLight",
		TileProvider:           staticmaps.NewTileProviderCartoLight(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "OpenCycleMap",
		TileProvider:           staticmaps.NewTileProviderOpenCycleMap(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "OpenStreetMaps",
		TileProvider:           staticmaps.NewTileProviderOpenStreetMaps(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "OpenTopoMap",
		TileProvider:           staticmaps.NewTileProviderOpenTopoMap(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},

	/**
	 * TODO:
	 * - [] The tileset doesn't work for hgh resolution like iPAD
	 */
	TileProvider{
		Name:                   "StamenTerrain",
		TileProvider:           staticmaps.NewTileProviderStamenTerrain(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "StamenToner",
		TileProvider:           staticmaps.NewTileProviderStamenToner(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff}, // Black
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "ThunderforestLandscape",
		TileProvider:           staticmaps.NewTileProviderThunderforestLandscape(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "ThunderforestOutdoors",
		TileProvider:           staticmaps.NewTileProviderThunderforestOutdoors(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "ThunderforestTransport",
		TileProvider:           staticmaps.NewTileProviderThunderforestTransport(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	TileProvider{
		Name:                   "Wikimedia",
		TileProvider:           staticmaps.NewTileProviderWikimedia(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors:     ActivityTypeColors,
	},
	/**
	 * TODO:
	 * - [] The tileset doesn't work for hgh resolution like iPAD
	 */
	TileProvider{
		Name:                   "StamenWatercolor",
		TileProvider:           NewTileProviderStamenWatercolor(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
		activityTypeColors: [...]color.Color{
			color.RGBA{242, 60, 80, 0xff},   // Strava Type 0: Default? -> Assumption: The gpx is a running activity; so use the running color from 9
			color.RGBA{0, 0, 0, 0xff},       // Strava Type 1: Cycling - yellow
			color.RGBA{255, 255, 255, 0xff}, // Strava Type 2: ? - white
			color.RGBA{255, 255, 255, 0xff}, // Strava Type 3: ? - white
			color.RGBA{6, 149, 171, 0xff},   // Strava Type 4: Hiking - blue
			color.RGBA{255, 255, 255, 0xff}, // Strava Type 5: ? - white
			color.RGBA{255, 255, 255, 0xff}, // Strava Type 6: ? - white
			color.RGBA{255, 255, 255, 0xff}, // Strava Type 7: ? - white
			color.RGBA{255, 255, 255, 0xff}, // Strava Type 8: ? - white
			color.RGBA{242, 60, 80, 0xff},   // Strava Type 9: Running - pink/red
			color.RGBA{69, 196, 139, 0xff},  // Strava Type 10: Walking - green
		},
	},
	TileProvider{
		Name:                   "Default",
		TileProvider:           staticmaps.NewTileProviderCartoDark(),
		fontActivityTypeColor:  color.RGBA{255, 255, 255, 0xff},
		fontActivityStatsColor: color.RGBA{0, 0, 0, 0xff},
		fontActivityDateColor:  color.RGBA{255, 255, 255, 0xff},
		fontLogoColor:          color.RGBA{0, 0, 0, 0xff},
	},
}
