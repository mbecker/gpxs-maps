module gitlab.com/mbecker/gpxs-maps

go 1.12

require (
	github.com/Wessie/appdirs v0.0.0-20141031215813-6573e894f8e2 // indirect
	github.com/alexsasharegan/dotenv v0.0.0-20171113213728-090a4d1b5d42
	github.com/flopp/go-coordsparser v0.0.0-20160810104536-845bca739e26 // indirect
	github.com/flopp/go-staticmaps v0.0.0-20180404185116-320790ed5329
	github.com/fogleman/gg v1.1.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/golang/geo v0.0.0-20181008215305-476085157cff
	github.com/golang/protobuf v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.0.0
	github.com/lucasb-eyer/go-colorful v0.0.0-20180526135729-345fbb3dbcdb
	github.com/mbecker/gpxs v0.0.0-20181125080705-0e0d2c584943
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/rs/xid v1.2.1
	github.com/rs/zerolog v1.11.0
	github.com/satori/go.uuid v1.2.0
	github.com/sendgrid/rest v2.4.1+incompatible
	github.com/sendgrid/sendgrid-go v3.4.1+incompatible
	github.com/streadway/amqp v0.0.0-20181107104731-27835f1a64e9 // indirect
	github.com/tkrajina/gpxgo v1.0.0 // indirect
	gitlab.com/mbecker/gpxs-amqp-lib v0.0.0-20190214125029-83fb634c18fd
	gitlab.com/mbecker/gpxs-proto v0.0.0-20190221151518-70aeea92ae9c
	golang.org/x/image v0.0.0-20181116024801-cd38e8056d9b // indirect
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
