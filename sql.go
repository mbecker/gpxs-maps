package main

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/lib/pq"
	"github.com/mbecker/gpxs/geo"
	"github.com/rs/zerolog/log"
	"github.com/satori/go.uuid"
)

type Privacy struct {
	IsPrivacyPublic    bool
	IsPrivacyCommunity bool
	IsPrivacyPrivate   bool
}

// ActivityImagesStatus defines the path and the saved status of an image
type ActivityImagesStatus struct {
	FileName string
	Path     string
	Width    float64
	Height   float64
	Saved    bool
}

// ActivityImages includes all images and their status
type ActivityImages struct {
	ImagePreset      *ImagePreset
	ActivityTpe      int32
	ActivityTypeName string
	Filename         string
	Original         ActivityImagesStatus
	Resized          ActivityImagesStatus
	Thumb            ActivityImagesStatus
	CreatedAt        time.Time
}

func sqlStrava(
	db *sql.DB,
	activityID int64,
	mapID string,
	uuid uuid.UUID,
	activityImages *ActivityImages,
	privacy *Privacy,
) (string, error) {
	defer timeTrack(time.Now(), "DB sqlStrava", mapID)

	var id string
	err := db.QueryRow(`
	INSERT INTO shard_1.maps
	(
		id, activity_id, uuid, activity_type, 
		filename, filepath, width, height, 
		resized_filename, resized_filepath, resized_width, resized_height, 
		thumb_filename, thumb_filepath, thumb_width, thumb_height, 
		type, preset, public, community, 
		private, created_at
	)
	VALUES
	(
		$1, $2, $3, $4, 
		$5, $6, $7, $8,
		$9, $10, $11, $12, 
		$13, $14, $15, $16,
		$17, $18, $19, $20,
		$21, $22
	) RETURNING id`,
		mapID, activityID, uuid, activityImages.ActivityTpe,
		activityImages.Original.FileName, activityImages.Original.Path, activityImages.Original.Width, activityImages.Original.Height,
		activityImages.Resized.FileName, activityImages.Resized.Path, activityImages.Resized.Width, activityImages.Resized.Height,
		activityImages.Thumb.FileName, activityImages.Thumb.Path, activityImages.Thumb.Width, activityImages.Thumb.Height,
		activityImages.ActivityTypeName, activityImages.ImagePreset.Name, privacy.IsPrivacyPublic, privacy.IsPrivacyCommunity,
		privacy.IsPrivacyPrivate, activityImages.CreatedAt,
	).Scan(&id)

	return id, err
}

func sqlInserts(
	db *sql.DB,
	gpxID int64,
	xidMap string,
	uuid uuid.UUID,
	gpxDoc *geo.GPX,
	activityName string,
	defaultActivityName string,
	activityType int,
	isPrivacyPublic bool,
	isPrivacyCommunity bool,
	isPrivacyPrivate bool,
	fileName string,
	filePath string,
	imageWidth float64,
	imageHeight float64,
	imageType string,
	imagePreset string,
	mapImageResizedName string,
	mapImageResizedFilePath string,
	mapImageResizedWidth float64,
	mapImageResizedHeight float64,

	mapImageThumbName string,
	mapImageThumbFilePath string,
	mapImageThumbWidth float64,
	mapImageThumbHeight float64,
	algorithmName string,
) error {
	defer timeTrack(time.Now(), "DB insertsDB", xidMap)

	// gpxID, err := sqlQueryRowGpxs(xid, db, uuid, gpxDoc, activityName, defaultActivityName, algorithmName)

	// if err != nil || gpxID == 0 {
	// 	log.Error().Str("xid", xid).Err(err).Msg("DB QueryRow GPX")
	// 	return err
	// }

	gpxName := gpxDoc.Name
	if len(gpxName) == 0 && len(activityName) > 0 {
		gpxName = defaultActivityName
	} else if len(gpxName) == 0 {
		gpxName = defaultActivityName
	}

	// activity_type: Extract the string value from gpx.Type
	gpxActivityType := getActivityTypeString(gpxDoc.Type)

	// duration_string: Format time of duration to string
	t01, _ := time.ParseDuration(fmt.Sprintf("%ds", int64(gpxDoc.MovementStats.MovingData.Duration)))

	// sqlStatements sould have all SQL statements which shoud be executed n one transaction
	var sqlStatements string

	var startTime time.Time
	var endTime time.Time
	if gpxDoc.MovementStats.OverallData.StartTime.Valid {
		startTime = *gpxDoc.MovementStats.OverallData.StartTime.Time
	}
	if gpxDoc.MovementStats.OverallData.EndTime.Valid {
		endTime = *gpxDoc.MovementStats.OverallData.EndTime.Time
	}

	sqlStatements += fmt.Sprintf(`
		UPDATE strava_gpxs
		SET
			name = '%s',
			activity_type = %d,
			distance = %f,
			duration = %f,
			duration_string = '%s',
			start_time = '%s',
			end_time = '%s'
		WHERE
			gpxid = %d;
			
			`, gpxName, gpxActivityType, gpxDoc.MovementStats.OverallData.Distance, gpxDoc.MovementStats.OverallData.Duration, t01, pq.FormatTimestamp(startTime),
		pq.FormatTimestamp(endTime), gpxID,
	)

	// INSERT Map SQL Statement
	// xid, gpxxid, uuid, public, community, private
	sqlStatements += fmt.Sprintf(`
	UPDATE strava_maps
	SET
		name = '%s',
		activity_type = %d,
		filename = '%s',
		filepath = '%s',
		type = '%s',
		preset = '%s',
		width = %f,
		height = %f,
		public = %t,
		community = %t,
		private = %t,
		resized_filename = '%s',
		resized_filepath = '%s',
		resized_width = %f,
		resized_height = %f,
		thumb_filename = '%s',
		thumb_filepath = '%s',
		thumb_width = %f,
		thumb_height = %f
	WHERE
		xid = '%s';`,
		activityName, activityType,
		fileName, filePath, imageType, imagePreset, imageWidth, imageHeight,
		isPrivacyPublic, isPrivacyCommunity, isPrivacyPrivate,
		mapImageResizedName, mapImageResizedFilePath, mapImageResizedWidth, mapImageResizedHeight,
		mapImageThumbName, mapImageThumbFilePath, mapImageThumbWidth, mapImageThumbHeight,
		xidMap,
	)

	// INSERT Image SQL Statement
	// sqlStatements += fmt.Sprintf("INSERT INTO strava_images (uuid, xid, filename, filepath, type, preset, width, height) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %f, %f);", uuid.String(), xid, filename, filepath, imageType, imagePreset, imageWidth, imageHeight)

	// INSERT MovementData SQL Statements
	movementData := map[int]geo.MovementData{
		0: gpxDoc.MovementStats.OverallData, // Key "0" is "overall data"
		1: gpxDoc.MovementStats.MovingData,  // Key "1" is "moving data"
		2: gpxDoc.MovementStats.StoppedData, // Key "2" is "stopped data"
	}
	sqlStatements += sqlStatementsMovementData(gpxID, movementData, algorithmName, xidMap)

	if err := sqlExec(xidMap, db, sqlStatements); err != nil {
		log.Error().Str("xid", xidMap).Err(err).Msg("DB Update Exec")
		return err
	}

	// INSERT Points SQL Stateent && INSERT Track FROM Points SQL Statement
	sqlStatements = sqlStatementsPointsAndTrack(gpxID, db, gpxDoc, xidMap)
	if err := sqlExec(xidMap, db, sqlStatements); err != nil {
		log.Error().Str("xid", xidMap).Err(err).Msg("DB Update Exec Points and Tracks")
		return err
	}

	return nil
}

func sqlExec(xidMap string, db *sql.DB, sqlStatement string) error {
	defer timeTrack(time.Now(), "(4) DB sqlQueryRowGpxs", xidMap)
	if _, err := db.Exec(sqlStatement); err != nil {
		return err
	}
	return nil
}

func sqlStatementsMovementData(gpxID int64, movementDatas map[int]geo.MovementData, algorithmName string, xid string) string {
	defer timeTrack(time.Now(), "(2) DB sqlStatementsMovementData", xid)

	var sqlStatements string
	for key, movementData := range movementDatas {

		var startTime time.Time
		var endTime time.Time
		if movementData.StartTime.Valid {
			startTime = *movementData.StartTime.Time
		}
		if movementData.EndTime.Valid {
			endTime = *movementData.EndTime.Time
		}

		t01, _ := time.ParseDuration(fmt.Sprintf("%ds", int64(movementData.Duration)))
		sqlStatements += fmt.Sprintf("INSERT INTO strava_movement_data ( gpxid, type, distance, duration, duration_string, start_time, end_time, max_speed, average_speed, max_pace, average_pace, min_latitude, max_latitude, min_longitude, max_longitude, min_elevation, max_elevation, algorithm) VALUES (%d, %d, %f, %f, '%s', '%s', '%s', %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, '%s');",
			gpxID, key,
			movementData.Distance, movementData.Duration, fmt.Sprintf("%s", t01),
			pq.FormatTimestamp(startTime), pq.FormatTimestamp(endTime),
			movementData.MaxSpeed, movementData.AverageSpeed, movementData.MaxPace, movementData.AveragePace,
			movementData.MinLatitude, movementData.MaxLatitude, movementData.MinLongitude, movementData.MaxLongitude,
			movementData.MinEvelation, movementData.MaxEvelation,
			algorithmName)
	}

	return sqlStatements
}

func sqlStatementsPointsAndTrack(gpxID int64, db *sql.DB, gpxs *geo.GPX, xidMap string) string {
	defer timeTrack(time.Now(), "(3) DB sqlStatementsPointsAndTrack", xidMap)
	now := time.Now()

	var sqlStatements string
	for trackNumber, trk := range gpxs.Tracks {
		for segmentNumber, seg := range trk.Segments {
			for pointNumber, point := range seg.Points {
				var elevation float64
				if point.Elevation.NotNull() {
					elevation = point.Elevation.Value()
				}
				sqlStatements += fmt.Sprintf("INSERT INTO strava_points (gpxid, trackid, segmentid, pointid, point, elevation, timestamp, created_at) VALUES (%d, %d, %d, %d, ST_SetSRID(ST_MakePoint(%f, %f),4326), %f, '%s', '%s');", gpxID, trackNumber, segmentNumber, pointNumber, point.GetLongitude(), point.GetLatitude(), elevation, pq.FormatTimestamp(*point.Timestamp.Time), pq.FormatTimestamp(now))
			}
		}
	}
	sqlStatements += fmt.Sprintf("insert into strava_tracks (gpxid, line) select gpxid, ST_MakeLine(point) AS line from strava_points WHERE gpxid = %d group by gpxid;", gpxID)
	// if _, err := db.Exec(sqlStatements); err != nil {
	// 	return err
	// }

	return sqlStatements
}
